# Projet ELE8702

## PyCharm

Un des meilleurs IDE Python qui existent, avec support pour Django.

PyCharm est un IDE __payant__ de JetBrains, mais il est gratuit pour les étudiants. Pour bénéficier, voir [ici](https://www.jetbrains.com/student/). Ensuite, pour télécharger PyCharm allez [ici](https://www.jetbrains.com/pycharm/download/#section=linux). Il y a aussi un tuto d'installation (sous le gros logo).

Pendant l'installation, faire attention à cocher la case qui permet de créer les liens (symlink). L'installateur ne crée pas d'icône par défaut, il faut en demander une à la première ouverture de l'IDE en allant dans Tools - Create Desktop Entry.

### Importation du projet dans PyCharm

Il faut commencer par créer un fork (une copie) du projet en allant sur la page Gitlab du projet (lamouette/intramwet).

Une fois le fork créé, copier l'url qui se situe à côté du bouton fork et ouvrir PyCharm. Il faut ensuite faire 'Check out from version control' (menu `VSC` en haut), ce qui permet d'importer le projet (il faut se loger avec son nom d'utilisateur et mot de passe Gitlab).


### Manupulation de PyCharm

C'est assez simple en fait, il suffit de faire `Run` (Shift+F10) et le programme se lance.

## Git

Pour git, c'est tout intégré dans l'IDE; vous pouvez directement faire les commits en appuyant sur l'icône Commit en haut a droite (ou bien avec le racourci Ctrl+K). Vous pouvez faire `Commit`, ou `Commit+Push` qui envoit directement vos modifications au serveur distant Gitlab. Vous pouvez mettre à jour votre projet depuis la branche principale avec `Update` (ou Ctrl+T).

Pour les Commits, faites-en le plus souvent possible (un commit est reversible, ça vous permet de revenir en arrière en cas de problèmes). Et puis ça aide à voire exactement ce qui a été changé depuis le dernier commit.

Pour un client Git bien plus puissant, vous pouvez installer [Gitkraken](https://www.gitkraken.com/).


## Python3

### Linux

Pour les distributions basées sur Debian/Ubuntu:

```bash
sudo apt install python3 python3-pip
```

### Windows

Un tutoriel [ici](https://www.howtogeek.com/197947/how-to-install-python-on-windows/)

### Mac

Un tutoriel [ici](http://docs.python-guide.org/en/latest/starting/install3/osx/)